package com.expedia.exercise.bclock.core.model.lights;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static com.expedia.exercise.bclock.core.model.lights.LightIndicator.MINUTE_5;

public class LightsFactory {
    public static final int HOURS_BLOCK_LAST_INDEX = 4;
    public static final int MINUTE_LAST_INDEX = 11;

    public static final String DELIMITER = "|";

    public Map<Integer, List<String>> lightsFrom(final Map<LightIndicator, Integer> register, final int second) {
        final Map<Integer, List<String>> builder = new HashMap<>();

        builder.put(1, secondsLightSwitch(second));
        builder.put(2, doIt(register, LightIndicator.HOUR_5));
        builder.put(3, doIt(register, LightIndicator.HOUR_1));
        builder.put(4, all5MinuteBlocksLit(register));
        builder.put(5, doIt(register, LightIndicator.MINUTE_1));

        return builder;
    }

    private List<String> secondsLightSwitch(final int second) {
        return Collections.singletonList(isEven(second) ? LightIndicator.SECOND.on() : LightIndicator.SECOND.off());
    }

    private boolean isEven(int seconds) {
        return seconds % 2 == 0;
    }

    private List<String> doIt(Map<LightIndicator, Integer> register, LightIndicator indicator) {
        final List<String> clockRow = new ArrayList<>();

        final Integer qtyLightsOn = register.get(indicator);
        IntStream.range(0, qtyLightsOn).forEach(index -> clockRow.add(indicator.on()));

        final Integer qtyLightsOff = HOURS_BLOCK_LAST_INDEX - qtyLightsOn;
        IntStream.range(0, qtyLightsOff).forEach(index -> clockRow.add(indicator.off()));

        return clockRow;
    }

    private List<String> all5MinuteBlocksLit(Map<LightIndicator, Integer> register) {
        final List<String> list = new ArrayList<>();

        final Integer qtyLightsOn = register.get(MINUTE_5);
        IntStream.range(0, qtyLightsOn).forEach(index -> list.add(MINUTE_5.onFor(index)));

        final Integer qtyLightsOff = MINUTE_LAST_INDEX - qtyLightsOn;
        IntStream.range(0, qtyLightsOff).forEach(index -> list.add((MINUTE_5.off())));

        return list;
    }
}
