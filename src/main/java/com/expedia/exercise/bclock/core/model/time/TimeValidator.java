package com.expedia.exercise.bclock.core.model.time;

public class TimeValidator {

    static final int MAXIMUM_POSSIBLE_HOUR = 24;
    static final int MAXIMUM_POSSIBLE_MINUTE = 59;
    static final int MINIMUM_POSSIBLE_HOUR = 1;
    static final int MINIMUM_POSSIBLE_MINUTE = 0;
    static final int MAXIMUM_POSSIBLE_SECOND = 59;
    static final int MINIMUM_POSSIBLE_SECOND = 0;
    static final String MAXIMUM_HOUR_OVERFLOW_ERROR_MESSAGE = "Maximum of 24 hours allowed";
    static final String TOO_BIG_MINUTE_ERROR_MESSAGE = "Maximum of 59 minutes allowed";
    static final String TOO_SMALL_MINUTE_ERROR_MESSAGE = "Minimum of 0 minutes allowed";
    static final String MINIMUM_HOUR_ERROR_MESSAGE = "Hours less than 1 are not allowed";
    static final String MAXIMUM_POSSIBLE_SECOND_ERROR_MESSAGE = "Seconds greater than 59 are not allowed" ;
    static final String MINIMUM_POSSIBLE_SECOND_ERROR_MESSAGE = "Seconds smaller than 0 are not allowed" ;

    public void validate(final int hour, final int minutes, final int seconds) {
        if (hour > MAXIMUM_POSSIBLE_HOUR) {
            throw new IllegalArgumentException(MAXIMUM_HOUR_OVERFLOW_ERROR_MESSAGE);
        }

        if (hour < MINIMUM_POSSIBLE_HOUR) {
            throw new IllegalArgumentException(MINIMUM_HOUR_ERROR_MESSAGE);
        }

        if (minutes > MAXIMUM_POSSIBLE_MINUTE) {
            throw new IllegalArgumentException(TOO_BIG_MINUTE_ERROR_MESSAGE);
        }

        if (minutes < MINIMUM_POSSIBLE_MINUTE) {
            throw new IllegalArgumentException(TOO_SMALL_MINUTE_ERROR_MESSAGE);
        }

        if (seconds > MAXIMUM_POSSIBLE_SECOND) {
            throw new IllegalArgumentException(MAXIMUM_POSSIBLE_SECOND_ERROR_MESSAGE);
        }

        if(seconds < MINIMUM_POSSIBLE_SECOND) {
            throw new IllegalArgumentException(MINIMUM_POSSIBLE_SECOND_ERROR_MESSAGE);
        }
    }
}
