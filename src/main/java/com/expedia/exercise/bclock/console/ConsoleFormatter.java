package com.expedia.exercise.bclock.console;

import com.expedia.exercise.bclock.core.model.lights.LightsFactory;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

public class ConsoleFormatter {

    private static final int MAXIMUM_LENGTH_OF_LINE = 110;
    private static final String NEW_LINE = "\n";

    public String format(final Map<Integer, List<String>> clockDisplay) {
        final StringBuilder clockBuilder = new StringBuilder();

        clockDisplay.forEach((line, lights) -> {
            final StringBuilder lineBuilder = new StringBuilder();
            lights.forEach(light -> lineBuilder.append(wrap(light)));
            clockBuilder.append(pad(lineBuilder.toString())).append(NEW_LINE);
        });

        return clockBuilder.toString();
    }

    private String wrap(final String content) {
        return LightsFactory.DELIMITER + content;
    }

    private String pad(final String line) {
        return StringUtils.center(line + LightsFactory.DELIMITER, MAXIMUM_LENGTH_OF_LINE);
    }

}