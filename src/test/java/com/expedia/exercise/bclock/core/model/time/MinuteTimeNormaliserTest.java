package com.expedia.exercise.bclock.core.model.time;

import com.expedia.exercise.bclock.core.actions.TestConstants;
import com.expedia.exercise.bclock.core.model.lights.LightIndicator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class MinuteTimeNormaliserTest {

    @InjectMocks
    private TimeNormaliser normaliser;

    private Map<LightIndicator, Integer> register;

    @Test
    public void should_allocate_0_5minute_lights_for_0_minutes() {
        register = normaliser.normalise(TestConstants.HOUR_01, TestConstants.MINUTES_00);

        assertThat(register).containsEntry(LightIndicator.MINUTE_5, 0);
    }

    @Test
    public void should_allocate_0_1minute_lights_for_0_minutes() {
        register = normaliser.normalise(TestConstants.HOUR_01, TestConstants.MINUTES_00);

        assertThat(register).containsEntry(LightIndicator.MINUTE_1, 0);
    }

    @Test
    public void should_allocate_6_5minute_lights_for_34_minutes() {
        register = normaliser.normalise(TestConstants.HOUR_01, TestConstants.MINUTES_34);

        assertThat(register).containsEntry(LightIndicator.MINUTE_5, 6);
    }

    @Test
    public void should_allocate_4_1minute_lights_for_34_minutes() {
        register = normaliser.normalise(TestConstants.HOUR_01, TestConstants.MINUTES_34);

        assertThat(register).containsEntry(LightIndicator.MINUTE_1, 4);
    }

    @Test
    public void should_allocate_11_5minute_lights_for_59_minutes() {
        register = normaliser.normalise(TestConstants.HOUR_01, TestConstants.MINUTES_59);

        assertThat(register).containsEntry(LightIndicator.MINUTE_5, 11);
    }

    @Test
    public void should_allocate_4_1minute_lights_for_59_minutes() {
        register = normaliser.normalise(TestConstants.HOUR_01, TestConstants.MINUTES_59);

        assertThat(register).containsEntry(LightIndicator.MINUTE_1, 4);
    }
}
