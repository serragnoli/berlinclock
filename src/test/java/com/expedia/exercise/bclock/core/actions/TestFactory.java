package com.expedia.exercise.bclock.core.actions;

import com.expedia.exercise.bclock.core.model.lights.LightsFactory;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.expedia.exercise.bclock.core.model.lights.LightIndicator.HOUR_1;
import static com.expedia.exercise.bclock.core.model.lights.LightIndicator.HOUR_5;
import static com.expedia.exercise.bclock.core.model.lights.LightIndicator.MINUTE_1;
import static com.expedia.exercise.bclock.core.model.lights.LightIndicator.MINUTE_5;
import static com.expedia.exercise.bclock.core.model.lights.LightIndicator.SECOND;

public class TestFactory {

    public static final int MAXIMUM_LENGTH_OF_LINE = 110;
    public static final String NEW_LINE = "\n";

    public static Map<Integer, List<String>> allLightsLitMap() {
        final Map<Integer, List<String>> results = new HashMap<>();
        int idx = 0;

        results.put(1, Collections.singletonList(SECOND.on()));
        results.put(2, Arrays.asList(HOUR_5.on(), HOUR_5.on(), HOUR_5.on(), (HOUR_5.on())));
        results.put(3, Arrays.asList(HOUR_1.on(), HOUR_1.on(), HOUR_1.on(), HOUR_1.on()));
        results.put(4, Arrays.asList(MINUTE_5.onFor(idx++), MINUTE_5.onFor(idx++), MINUTE_5.onFor(idx++),
                MINUTE_5.onFor(idx++), MINUTE_5.onFor(idx++), MINUTE_5.onFor(idx++),
                MINUTE_5.onFor(idx++), MINUTE_5.onFor(idx++), MINUTE_5.onFor(idx++), MINUTE_5.onFor(idx++),
                MINUTE_5.onFor(idx)));
        results.put(5, Arrays.asList(MINUTE_1.on(), MINUTE_1.on(), MINUTE_1.on(), MINUTE_1.on()));

        return results;
    }

    public static Map<Integer, List<String>> oneInTheMorningOnlyOneLightLitMap() {
        final Map<Integer, List<String>> result = new HashMap<>();

        result.put(1, Collections.singletonList(SECOND.off()));
        result.put(2, Arrays.asList(HOUR_5.off(), HOUR_5.off(), HOUR_5.off(), HOUR_5.off()));
        result.put(3, Arrays.asList(HOUR_1.on(), HOUR_1.off(), HOUR_1.off(), HOUR_1.off()));
        result.put(4, Arrays.asList(MINUTE_5.off(), MINUTE_5.off(), MINUTE_5.off(),
                MINUTE_5.off(), MINUTE_5.off(), MINUTE_5.off(),
                MINUTE_5.off(), MINUTE_5.off(), MINUTE_5.off(),
                MINUTE_5.off(), MINUTE_5.off()));
        result.put(5, Arrays.asList(MINUTE_1.off(), MINUTE_1.off(), MINUTE_1.off(), MINUTE_1.off()));

        return result;
    }

    public static Map<Integer, List<String>> twelveThirtyTwoHalfOfLightsLitMap() {
        final Map<Integer, List<String>> lights = new HashMap<>();
        int idx = 0;
        lights.put(1, Collections.singletonList(SECOND.on()));
        lights.put(2, Arrays.asList(HOUR_5.on(), HOUR_5.on(), HOUR_5.off(), HOUR_5.off()));
        lights.put(3, Arrays.asList(HOUR_1.on(), HOUR_1.on(), HOUR_1.off(), HOUR_1.off()));
        lights.put(4, Arrays.asList(MINUTE_5.onFor(idx++), MINUTE_5.onFor(idx++), MINUTE_5.onFor(idx++),
                MINUTE_5.onFor(idx++), MINUTE_5.onFor(idx++), MINUTE_5.onFor(idx), MINUTE_5.off(),
                MINUTE_5.off(), MINUTE_5.off(), MINUTE_5.off(), MINUTE_5.off()));
        lights.put(5, Arrays.asList(MINUTE_1.on(), MINUTE_1.on(), MINUTE_1.off(), MINUTE_1.off()));

        return lights;
    }

    public static String twelveThirtyTwoPmHalfOfLightsLitFormatted() {
        int idx = 0;

        return line((prep(SECOND.on()))) +
                line(prep(HOUR_5.on()) + prep(HOUR_5.on()) + prep(HOUR_5.off()) + prep(HOUR_5.off())) +
                line(prep(HOUR_1.on()) + prep(HOUR_1.on()) + prep(HOUR_1.off()) + prep(HOUR_1.off())) +
                line(prep(MINUTE_5.onFor(idx++)) + prep(MINUTE_5.onFor(idx++)) + prep(MINUTE_5.onFor(idx++)) +
                        prep(MINUTE_5.onFor(idx++)) + prep(MINUTE_5.onFor(idx++)) + prep(MINUTE_5.onFor(idx)) +
                        prep(MINUTE_5.off()) + prep(MINUTE_5.off()) + prep(MINUTE_5.off()) +
                        prep(MINUTE_5.off()) + prep(MINUTE_5.off())) +
                line(prep(MINUTE_1.on()) + prep(MINUTE_1.on()) + prep(MINUTE_1.off()) + prep(MINUTE_1.off()));
    }

    public static Map<Integer, List<String>> twelveThirtyTwoPmHalfOfLightsLitFormattedMap() {
        final Map<Integer, List<String>> result = new HashMap<>();
        int idx = 0;

        result.put(1, Collections.singletonList(SECOND.on()));
        result.put(2, Arrays.asList(HOUR_5.on(), HOUR_5.on(), HOUR_5.off(), HOUR_5.off()));
        result.put(3, Arrays.asList(HOUR_1.on(), HOUR_1.on(), HOUR_1.off(), HOUR_1.off()));
        result.put(4, Arrays.asList(MINUTE_5.onFor(idx++), MINUTE_5.onFor(idx++), MINUTE_5.onFor(idx++),
                MINUTE_5.onFor(idx++), MINUTE_5.onFor(idx++), MINUTE_5.onFor(idx),
                MINUTE_5.off(), MINUTE_5.off(), MINUTE_5.off(),
                MINUTE_5.off(), MINUTE_5.off()));
        result.put(5, Arrays.asList(MINUTE_1.on(), MINUTE_1.on(), MINUTE_1.off(), MINUTE_1.off()));

        return result;
    }

    private static String prep(final String value) {
        return LightsFactory.DELIMITER + value;
    }

    private static String line(final String value) {
        return StringUtils.center(value + LightsFactory.DELIMITER, MAXIMUM_LENGTH_OF_LINE) + NEW_LINE;
    }
}