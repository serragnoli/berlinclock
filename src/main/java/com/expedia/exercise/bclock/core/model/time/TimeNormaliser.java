package com.expedia.exercise.bclock.core.model.time;

import com.expedia.exercise.bclock.core.model.lights.LightIndicator;

import java.util.HashMap;
import java.util.Map;

public class TimeNormaliser {

    public static final int MAXIMUM_VALUE_LIGHT_IS_WORTH = 5;

    public Map<LightIndicator, Integer> normalise(final int hour, final int minute) {
        final Map<LightIndicator, Integer> register = new HashMap<>();

        normaliseBlockWorth5(register, LightIndicator.HOUR_5, hour);
        normaliseBlockWorth5(register, LightIndicator.MINUTE_5, minute);

        normaliseBlockWorth1(register, LightIndicator.HOUR_1, hour);
        normaliseBlockWorth1(register, LightIndicator.MINUTE_1, minute);

        return register;
    }

    private void normaliseBlockWorth5(final Map<LightIndicator, Integer> register, final LightIndicator state, final Integer timeUnit) {
        final Integer lightsForCurrentState = timeUnit / MAXIMUM_VALUE_LIGHT_IS_WORTH;

        register.put(state, lightsForCurrentState);
    }

    private void normaliseBlockWorth1(final Map<LightIndicator, Integer> register, final LightIndicator state, final Integer timeUnit) {
        final Integer lightsForCurrentState = timeUnit % MAXIMUM_VALUE_LIGHT_IS_WORTH;

        register.put(state, lightsForCurrentState);
    }
}
