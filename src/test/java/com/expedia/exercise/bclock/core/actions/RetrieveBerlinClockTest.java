package com.expedia.exercise.bclock.core.actions;

import com.expedia.exercise.bclock.core.model.lights.LightIndicator;
import com.expedia.exercise.bclock.core.model.lights.LightsService;
import com.expedia.exercise.bclock.core.model.time.TimeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.expedia.exercise.bclock.core.actions.TestConstants.HOUR_24;
import static com.expedia.exercise.bclock.core.actions.TestConstants.MINUTES_59;
import static com.expedia.exercise.bclock.core.actions.TestConstants.SECONDS_58;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class RetrieveBerlinClockTest {

    @Mock
    private TimeService timeService;

    @Mock
    private LightsService lightsService;

    @InjectMocks
    private RetrieveBerlinClock retrieveBerlinClock;

    @Test
    public void should_invoke_time_service() {
        retrieveBerlinClock.retrieveBerlinClockWith(HOUR_24, MINUTES_59, SECONDS_58);

        verify(timeService).transform(HOUR_24, MINUTES_59, SECONDS_58);
    }

    @Test
    public void shoul_invoke_lights_service() {
        retrieveBerlinClock.retrieveBerlinClockWith(HOUR_24, MINUTES_59, SECONDS_58);

        verify(lightsService).convert(anyMapOf(LightIndicator.class, Integer.class), eq(SECONDS_58));
    }

    @Test
    public void should_invoke_collaborators_in_sequence() {
        retrieveBerlinClock.retrieveBerlinClockWith(HOUR_24, MINUTES_59, SECONDS_58);

        InOrder inOrder = inOrder(timeService, lightsService);
        inOrder.verify(timeService).transform(HOUR_24, MINUTES_59, SECONDS_58);
        inOrder.verify(lightsService).convert(anyMapOf(LightIndicator.class, Integer.class), eq(SECONDS_58));
    }
}