package com.expedia.exercise.bclock.core.actions;

import com.expedia.exercise.bclock.core.model.lights.LightsFactory;
import com.expedia.exercise.bclock.core.model.lights.LightsService;
import com.expedia.exercise.bclock.core.model.time.TimeNormaliser;
import com.expedia.exercise.bclock.core.model.time.TimeService;
import com.expedia.exercise.bclock.core.model.time.TimeValidator;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static com.expedia.exercise.bclock.core.actions.TestConstants.HOUR_01;
import static com.expedia.exercise.bclock.core.actions.TestConstants.HOUR_12;
import static com.expedia.exercise.bclock.core.actions.TestConstants.HOUR_24;
import static com.expedia.exercise.bclock.core.actions.TestConstants.MINUTES_00;
import static com.expedia.exercise.bclock.core.actions.TestConstants.MINUTES_32;
import static com.expedia.exercise.bclock.core.actions.TestConstants.MINUTES_59;
import static com.expedia.exercise.bclock.core.actions.TestConstants.SECONDS_01;
import static com.expedia.exercise.bclock.core.actions.TestConstants.SECONDS_58;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


public class RetrieveBerlinClockFeatureTest {

    private RetrieveBerlinClock retrieveBerlinClock;

    @Before
    public void set_up() {
        final TimeValidator validator = new TimeValidator();
        final TimeNormaliser normaliser = new TimeNormaliser();
        final LightsFactory factory = new LightsFactory();

        final TimeService timeService = new TimeService(validator, normaliser);
        final LightsService lightsService = new LightsService(factory);

        retrieveBerlinClock = new RetrieveBerlinClock(timeService, lightsService);
    }

    @Test
    public void should_display_2_seconds_to_1am() {
        final Map<Integer, List<String>> result = retrieveBerlinClock.retrieveBerlinClockWith(HOUR_24, MINUTES_59, SECONDS_58);

        assertThat(result).isEqualTo(TestFactory.allLightsLitMap());
    }

    @Test
    public void should_display_1_second_past_1am() {
        final Map<Integer, List<String>> result = retrieveBerlinClock.retrieveBerlinClockWith(HOUR_01, MINUTES_00, SECONDS_01);

        assertThat(result).isEqualTo(TestFactory.oneInTheMorningOnlyOneLightLitMap());
    }

    @Test
    public void should_diplay_12_32am() {
        final Map<Integer, List<String>> result = retrieveBerlinClock.retrieveBerlinClockWith(HOUR_12, MINUTES_32, SECONDS_58);

        assertThat(result).isEqualTo(TestFactory.twelveThirtyTwoPmHalfOfLightsLitFormattedMap());
    }
}