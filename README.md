# Tech Stack
* Java 8 (1.8.0_66)
* Maven 3 (3.3.3)

# Running the app
## From the IDE
1. Run the main method in `com.expedia.exercise.bclock.console.BerlinClock`
## From the command line
1. Run `mvn clean compile assembly:single`
2. Execute the app `java -jar target/bclock-1.0.0-SNAPSHOT-jar-with-dependencies.jar`

# Requirement:
* Implement the Berlin Clock as a function of the three parameter hours (24-based), minutes, seconds and return a multi line string.
* Find a reasonable representation for the colors and states.

# Berlin Clock description:
* The time is calculated by adding the lit rectangular lamps.
* The top lamp is a pump which is blinking on/off every two seconds.
* In the upper line of red lamps every lamp represents 5 hours.
* In the lower line of red lamps every lamp represents 1 hour.
* So if in the first line 2 lamps are lit and in the second line 3 lamps its 5+5+3=13h or 1 p.m.
* In the third line with tall lamps every lamp represents 5 minutes.
* There are 11 lamps, the 3rd, 6th, and 9th are red indicating the first quarter, half, and the last quarter of the hour.
* In the last line with yellow lamps every lamp represents 1 minute.

# Further source of information:
* https://en.wikipedia.org/wiki/Mengenlehreuhr

# Structure of the project:
## Delivery strategy
### console
 * This is the delivery mechanism for this domain.

## Domain
### core
 * Contains the domain of the application.
 * This is supposed to be reusable for different delivery mechanisms i.e. Rest, Web etc.

### core/actions
 * All the system can do is documented by classes at this level.
 * This is effectivelly the entry point of the application

### core/model
 * This contains the concepts of this domain
 * I have identified only think of two domain concepts for this application: TIME and LIGHTS