package com.expedia.exercise.bclock.core.model.time;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static com.expedia.exercise.bclock.core.actions.TestConstants.HOUR_00;
import static com.expedia.exercise.bclock.core.actions.TestConstants.HOUR_23;
import static com.expedia.exercise.bclock.core.actions.TestConstants.HOUR_24;
import static com.expedia.exercise.bclock.core.actions.TestConstants.MINUTES_00;
import static com.expedia.exercise.bclock.core.actions.TestConstants.MINUTES_59;
import static com.expedia.exercise.bclock.core.actions.TestConstants.SECONDS_00;
import static com.expedia.exercise.bclock.core.actions.TestConstants.SECONDS_58;
import static com.expedia.exercise.bclock.core.actions.TestConstants.SECONDS_59;
import static com.expedia.exercise.bclock.core.actions.TestConstants.TOO_BIG_HOUR;
import static com.expedia.exercise.bclock.core.actions.TestConstants.TOO_BIG_MINUTE;
import static com.expedia.exercise.bclock.core.actions.TestConstants.TOO_BIG_SECOND;
import static com.expedia.exercise.bclock.core.actions.TestConstants.TOO_SMALL_HOUR;
import static com.expedia.exercise.bclock.core.actions.TestConstants.TOO_SMALL_MINUTE;
import static com.expedia.exercise.bclock.core.actions.TestConstants.TOO_SMALL_SECOND;
import static com.expedia.exercise.bclock.core.model.time.TimeValidator.MAXIMUM_HOUR_OVERFLOW_ERROR_MESSAGE;
import static com.expedia.exercise.bclock.core.model.time.TimeValidator.MINIMUM_HOUR_ERROR_MESSAGE;
import static com.expedia.exercise.bclock.core.model.time.TimeValidator.TOO_BIG_MINUTE_ERROR_MESSAGE;
import static com.expedia.exercise.bclock.core.model.time.TimeValidator.TOO_SMALL_MINUTE_ERROR_MESSAGE;
import static org.junit.rules.ExpectedException.none;

@RunWith(MockitoJUnitRunner.class)
public class TimeValidatorTest {

    @Rule
    public ExpectedException thrown = none();

    @InjectMocks
    private TimeValidator validator;

    @Test(expected = IllegalArgumentException.class)
    public void should_refuse_hour_greater_than_24() {
        validator.validate(TOO_BIG_HOUR, MINUTES_59, SECONDS_58);
    }

    @Test
    public void should_explain_hour_is_too_big_in_the_exception_message() {
        thrown.expectMessage(MAXIMUM_HOUR_OVERFLOW_ERROR_MESSAGE);
        validator.validate(TOO_BIG_HOUR, MINUTES_59, SECONDS_58);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_refuse_hour_smaller_than_1() {
        validator.validate(TOO_SMALL_HOUR, MINUTES_59, SECONDS_58);
    }

    @Test
    public void should_explain_hours_are_too_small_in_the_exception_message() {
        thrown.expectMessage(MINIMUM_HOUR_ERROR_MESSAGE);
        validator.validate(TOO_SMALL_HOUR, MINUTES_59, SECONDS_58);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_refuse_minutes_greater_than_59() {
        validator.validate(HOUR_24, TOO_BIG_MINUTE, SECONDS_58);
    }

    @Test
    public void should_explain_minute_is_too_big_in_the_exception_message() {
        thrown.expectMessage(TOO_BIG_MINUTE_ERROR_MESSAGE);
        validator.validate(HOUR_24, TOO_BIG_MINUTE, SECONDS_58);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_refuse_minutes_negative() {
        validator.validate(HOUR_24, TOO_SMALL_MINUTE, SECONDS_58);
    }

    @Test
    public void should_explain_minute_is_too_small_in_the_exception_message() {
        thrown.expectMessage(TOO_SMALL_MINUTE_ERROR_MESSAGE);
        validator.validate(HOUR_24, TOO_SMALL_MINUTE, SECONDS_58);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_refuse_seconds_bigger_than_59() {
        validator.validate(HOUR_24, MINUTES_59, TOO_BIG_SECOND);
    }

    @Test
    public void should_explain_second_is_too_big_in_the_exception_message() {
        thrown.expectMessage(TimeValidator.MAXIMUM_POSSIBLE_SECOND_ERROR_MESSAGE);
        validator.validate(HOUR_24, MINUTES_59, TOO_BIG_SECOND);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_refuse_seconds_negative() {
        validator.validate(HOUR_24, MINUTES_59, TOO_SMALL_SECOND);
    }

    @Test
    public void should_explain_second_is_too_small_in_the_exception_message() {
        thrown.expectMessage(TimeValidator.MINIMUM_POSSIBLE_SECOND_ERROR_MESSAGE);
        validator.validate(HOUR_24, MINUTES_59, TOO_SMALL_SECOND);
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_refuse_midnight_in_format_00_00() {
        validator.validate(HOUR_00, MINUTES_00, SECONDS_00);
    }

    @Test
    public void should_validate_midnight_in_format_24_00() {
        validator.validate(HOUR_24, MINUTES_00, SECONDS_00);
    }

    @Test
    public void should_validate_last_second_of_the_day() {
        validator.validate(HOUR_23, MINUTES_59, SECONDS_59);
    }
}