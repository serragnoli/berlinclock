package com.expedia.exercise.bclock.console;

import com.expedia.exercise.bclock.core.actions.RetrieveBerlinClock;

import java.util.List;
import java.util.Map;

public class BerlinClockPrinter {

    private final RetrieveBerlinClock retrieveBerlinClock;
    private final ConsoleFormatter formatter;
    private final Console console;

    public BerlinClockPrinter(final RetrieveBerlinClock retrieveBerlinClock,final ConsoleFormatter formatter, final Console console) {
        this.retrieveBerlinClock = retrieveBerlinClock;
        this.formatter = formatter;
        this.console = console;
    }

    public void print(final int hour, final int minute, final int second) {
        final Map<Integer, List<String>> clockLights = retrieveBerlinClock.retrieveBerlinClockWith(hour, minute, second);

        final String formattedTime = formatter.format(clockLights);

        console.print(formattedTime);
    }
}
