package com.expedia.exercise.bclock.console;

import com.expedia.exercise.bclock.core.actions.RetrieveBerlinClock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class BerlinClockPrinterTest {

    private static final int HOURS_24 = 12;
    private static final int MINUTES = 30;
    private static final int SECONDS = 15;

    @Mock
    private RetrieveBerlinClock retrieveBerlinClock;

    @Mock
    private Console console;

    @Mock
    private ConsoleFormatter formatter;

    @InjectMocks
    private BerlinClockPrinter consolePrinter;

    @Test
    public void should_invoke_retrieve_time_from_domain() {
        consolePrinter.print(HOURS_24, MINUTES, SECONDS);

        verify(retrieveBerlinClock).retrieveBerlinClockWith(HOURS_24, MINUTES, SECONDS);
    }

    @Test
    public void should_invoke_console_to_print_time() {
        consolePrinter.print(HOURS_24, MINUTES, SECONDS);

        verify(console).print(anyString());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void should_invoke_formatter() {
        consolePrinter.print(HOURS_24, MINUTES, SECONDS);

        verify(formatter).format(anyMap());
    }

    @Test
    public void should_invoke_collaborators_in_sequence() {
        consolePrinter.print(HOURS_24, MINUTES, SECONDS);

        final InOrder inOrder = inOrder(retrieveBerlinClock, console);
        inOrder.verify(retrieveBerlinClock).retrieveBerlinClockWith(HOURS_24, MINUTES, SECONDS);
        inOrder.verify(console).print(anyString());
    }
}
