package com.expedia.exercise.bclock.core.actions;

import com.expedia.exercise.bclock.core.model.lights.LightIndicator;
import com.expedia.exercise.bclock.core.model.lights.LightsService;
import com.expedia.exercise.bclock.core.model.time.TimeService;

import java.util.List;
import java.util.Map;

public class RetrieveBerlinClock {

    private TimeService timeService;
    private LightsService lightsService;

    public RetrieveBerlinClock(final TimeService timeService, final LightsService lightsService) {
        this.timeService = timeService;
        this.lightsService = lightsService;
    }

    public Map<Integer, List<String>> retrieveBerlinClockWith(int hour, int minute, int second) {
        final Map<LightIndicator, Integer> register = timeService.transform(hour, minute, second);

        return lightsService.convert(register, second);
    }
}
