package com.expedia.exercise.bclock.console;

import com.expedia.exercise.bclock.core.actions.RetrieveBerlinClock;
import com.expedia.exercise.bclock.core.model.lights.LightsFactory;
import com.expedia.exercise.bclock.core.model.lights.LightsService;
import com.expedia.exercise.bclock.core.model.time.TimeNormaliser;
import com.expedia.exercise.bclock.core.model.time.TimeService;
import com.expedia.exercise.bclock.core.model.time.TimeValidator;

import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class BerlinClock {

    private static final String INPUT_MESSAGE = "Input time (24-hour, minutes, seconds): ";

    public static void main(String[] args) {
        final BerlinClockPrinter printer = buildAppDependencies();

        inputCollector(printer);
    }

    private static BerlinClockPrinter buildAppDependencies() {
        final TimeValidator validator = new TimeValidator();
        final TimeNormaliser normaliser = new TimeNormaliser();
        final TimeService timeService = new TimeService(validator, normaliser);

        final LightsFactory factory = new LightsFactory();
        final LightsService lightsService = new LightsService(factory);

        final RetrieveBerlinClock retriever = new RetrieveBerlinClock(timeService, lightsService);
        final ConsoleFormatter formatter = new ConsoleFormatter();
        final Console console = new Console();

        return new BerlinClockPrinter(retriever, formatter, console);
    }

    private static void inputCollector(BerlinClockPrinter printer) {
        System.out.print(INPUT_MESSAGE);
        Scanner input = new Scanner(System.in);
        String textualDate;
        while ((textualDate = input.nextLine()) != null) {
            final String[] timeText = textualDate.split(",");

            printer.print(parseInt(clean(timeText[0])), parseInt(clean(timeText[1])), parseInt(clean(timeText[2])));

            System.out.print(INPUT_MESSAGE);
        }
    }

    private static String clean(final String text) {
        return text.trim();
    }
}
