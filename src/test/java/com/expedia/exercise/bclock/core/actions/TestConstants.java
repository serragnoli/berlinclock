package com.expedia.exercise.bclock.core.actions;

public final class TestConstants {

    public static final int HOUR_00 = 0;
    public static final int HOUR_01 = 1;
    public static final int HOUR_04 = 4;
    public static final int HOUR_06 = 6;
    public static final int HOUR_12 = 12;
    public static final int HOUR_19 = 19;
    public static final int HOUR_20 = 20;
    public static final int HOUR_21 = 21;
    public static final int HOUR_23 = 23;
    public static final int HOUR_24 = 24;
    public static final int TOO_BIG_HOUR = 25;

    public static final int TOO_SMALL_HOUR = 0;
    public static final int MINUTES_00 = 0;
    public static final int MINUTES_32 = 32;
    public static final int MINUTES_34 = 34;
    public static final int MINUTES_59 = 59;

    public static final int TOO_SMALL_MINUTE = -1;
    public static final int TOO_BIG_MINUTE = 60;
    public static final int SECONDS_00 = 0;
    public static final int SECONDS_01 = 1;
    public static final int SECONDS_58 = 58;
    public static final int SECONDS_59 = 59;
    public static final int TOO_BIG_SECOND = 60;
    public static final int TOO_SMALL_SECOND = -1;

    private TestConstants() {
    }
}
