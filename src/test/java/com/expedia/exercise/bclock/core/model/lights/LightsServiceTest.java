package com.expedia.exercise.bclock.core.model.lights;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LightsServiceTest {

    @Mock
    private LightsFactory factory;

    @InjectMocks
    private LightsService lightsService;

    @Test
    public void should_invoke_lights_factory() {
        final Map<LightIndicator, Integer> register = anyMapOf(LightIndicator.class, Integer.class);
        final int second = anyInt();

        lightsService.convert(register, second);

        verify(factory).lightsFrom(register, second);
    }
}