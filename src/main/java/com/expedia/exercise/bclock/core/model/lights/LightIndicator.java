package com.expedia.exercise.bclock.core.model.lights;

import java.util.HashMap;
import java.util.Map;

public enum LightIndicator {
    SECOND("YELLOW-ON", "off"),
    HOUR_1("RED-ON", "off"),
    HOUR_5("RED-ON", "off"),
    MINUTE_1("YELLOW-ON", "off"),
    MINUTE_5("YELLOW-ON", "off") {
        @Override
        public String onFor(final int blockNumber) {
            final Map<Integer, String> theQuarterLight = new HashMap<>();
            theQuarterLight.put(2, "RED-ON");
            theQuarterLight.put(5, "RED-ON");
            theQuarterLight.put(8, "RED-ON");

            final String quarterLight = theQuarterLight.get(blockNumber);

            return quarterLight == null ? this.on() : quarterLight;
        }
    };

    private String on;
    private String off;

    LightIndicator(final String on, String off) {
        this.on = on;
        this.off = off;
    }

    public String on() {
        return this.on;
    }

    public String onFor(final int blockNumber) {
        return this.on;
    }

    public String off() {
        return this.off;
    }
}
