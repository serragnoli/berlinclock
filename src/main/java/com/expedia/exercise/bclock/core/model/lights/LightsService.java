package com.expedia.exercise.bclock.core.model.lights;

import java.util.List;
import java.util.Map;

public class LightsService {

    private final LightsFactory factory;

    public LightsService(final LightsFactory factory) {
        this.factory = factory;
    }

    public Map<Integer, List<String>> convert(final Map<LightIndicator, Integer> register, final int second) {
        return factory.lightsFrom(register, second);
    }
}
