package com.expedia.exercise.bclock.core.model.time;

import com.expedia.exercise.bclock.core.model.lights.LightIndicator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static com.expedia.exercise.bclock.core.actions.TestConstants.HOUR_01;
import static com.expedia.exercise.bclock.core.actions.TestConstants.HOUR_04;
import static com.expedia.exercise.bclock.core.actions.TestConstants.HOUR_06;
import static com.expedia.exercise.bclock.core.actions.TestConstants.HOUR_19;
import static com.expedia.exercise.bclock.core.actions.TestConstants.HOUR_20;
import static com.expedia.exercise.bclock.core.actions.TestConstants.HOUR_21;
import static com.expedia.exercise.bclock.core.actions.TestConstants.HOUR_24;
import static com.expedia.exercise.bclock.core.actions.TestConstants.MINUTES_00;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class HourTimeNormaliserTest {

    @InjectMocks
    private TimeNormaliser normaliser;

    private Map<LightIndicator, Integer> register;

    @Test
    public void should_allocate_0_5hour_lights_for_1am() {
        register = normaliser.normalise(HOUR_01, MINUTES_00);

        assertThat(register).containsEntry(LightIndicator.HOUR_5, 0);
    }

    @Test
    public void should_allocate_1_1hour_lights_for_1am() {
        register = normaliser.normalise(HOUR_01, MINUTES_00);

        assertThat(register).containsEntry(LightIndicator.HOUR_1, 1);
    }

    @Test
    public void should_allocate_0_5hour_lights_for_4am() {
        register = normaliser.normalise(HOUR_04, MINUTES_00);

        assertThat(register).containsEntry(LightIndicator.HOUR_5, 0);
    }

  @Test
    public void should_allocate_4_1hour_lights_for_4am() {
        register = normaliser.normalise(HOUR_04, MINUTES_00);

        assertThat(register).containsEntry(LightIndicator.HOUR_1, 4);
    }

    @Test
    public void should_allocate_1_5hour_lights_for_6am() {
        register = normaliser.normalise(HOUR_06, MINUTES_00);

        assertThat(register).containsEntry(LightIndicator.HOUR_5, 1);
    }

    @Test
    public void should_allocate_1_1hour_lights_for_6am() {
        register = normaliser.normalise(HOUR_06, MINUTES_00);

        assertThat(register).containsEntry(LightIndicator.HOUR_1, 1);
    }

    @Test
    public void should_allocate_3_5hour_lights_for_7pm() {
        register = normaliser.normalise(HOUR_19, MINUTES_00);

        assertThat(register).containsEntry(LightIndicator.HOUR_5, 3);
    }

    @Test
    public void should_allocate_4_5hour_lights_for_8pm() {
        register = normaliser.normalise(HOUR_20, MINUTES_00);

        assertThat(register).containsEntry(LightIndicator.HOUR_5, 4);
    }

    @Test
    public void should_allocate_4_5hour_lights_for_9pm() {
        register = normaliser.normalise(HOUR_21, MINUTES_00);

        assertThat(register).containsEntry(LightIndicator.HOUR_5, 4);
    }

    @Test
    public void should_allocate_4_5hour_lights_for_midnight() {
        register = normaliser.normalise(HOUR_24, MINUTES_00);

        assertThat(register).containsEntry(LightIndicator.HOUR_5, 4);
    }
}