package com.expedia.exercise.bclock.core.model.time;

import com.expedia.exercise.bclock.core.model.lights.LightIndicator;

import java.util.Map;

public class TimeService {

    private final TimeValidator validator;
    private final TimeNormaliser normaliser;

    public TimeService(final TimeValidator validator, final TimeNormaliser normaliser) {
        this.validator = validator;
        this.normaliser = normaliser;
    }

    public Map<LightIndicator, Integer> transform(final int hour, final int minute, final int second) {
        validator.validate(hour, minute, second);

        return normaliser.normalise(hour, minute);
    }
}
