package com.expedia.exercise.bclock.core.model.time;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.expedia.exercise.bclock.core.actions.TestConstants.HOUR_24;
import static com.expedia.exercise.bclock.core.actions.TestConstants.MINUTES_59;
import static com.expedia.exercise.bclock.core.actions.TestConstants.SECONDS_58;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TimeServiceTest {

    @Mock
    private TimeValidator validator;

    @Mock
    private TimeNormaliser normaliser;

    @InjectMocks
    private TimeService timeService;

    @Test
    public void should_invoke_time_validator() {
        timeService.transform(HOUR_24, MINUTES_59, SECONDS_58);

        verify(validator).validate(HOUR_24, MINUTES_59, SECONDS_58);
    }

    @Test
    public void should_invoke_time_normaliser() {
        timeService.transform(HOUR_24, MINUTES_59, SECONDS_58);

        verify(normaliser).normalise(HOUR_24, MINUTES_59);
    }

    @Test
    public void should_invoke_collaborators_in_sequence() {
        timeService.transform(HOUR_24, MINUTES_59, SECONDS_58);

        final InOrder inOrder = inOrder(validator, normaliser);
        inOrder.verify(validator).validate(HOUR_24, MINUTES_59, SECONDS_58);
        inOrder.verify(normaliser).normalise(HOUR_24, MINUTES_59);
    }
}