package com.expedia.exercise.bclock.console;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static com.expedia.exercise.bclock.core.actions.TestFactory.twelveThirtyTwoHalfOfLightsLitMap;
import static com.expedia.exercise.bclock.core.actions.TestFactory.twelveThirtyTwoPmHalfOfLightsLitFormatted;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ConsoleFormatterTest {

    @InjectMocks
    private ConsoleFormatter formatter;

    @Test
    public void should_pad_lines() {
        final String result = formatter.format(twelveThirtyTwoHalfOfLightsLitMap());

        assertThat(result).isEqualTo(twelveThirtyTwoPmHalfOfLightsLitFormatted());
    }
}
